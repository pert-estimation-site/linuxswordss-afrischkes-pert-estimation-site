var totsignature = '&#956;<sub>tot</sub>/&#963;<sub>tot</sub>';
var signature = ' (&#956;/&#963;)';
var taskcounter = 2;
var addTaskTo = function(row)
{
    var task = $('.cloner > li.task').clone(true);
    task.insertAfter(row);
    task.children('input:text').val('task ' + (taskcounter++));
}
var addTask = function(event)
{
    var source = $(event.explicitOriginalTarget).closest('li');
    addTaskTo(source);
    source.next('li.task').find('input.description').select();
}

var isNumber = function(candidate)
{
    return !isNaN(parseInt(candidate));
}

var popOutCSV = function()
{
    var header = 'description,O,N,P,duration,deviation\n';
    var data = header;
    $('.tasks ul li.task').each(
            function()
            {
                $(this).find('input').each(function(){
                data += $(this).val() + ',';
                });
                var mu = $(this).find('span.mu').html();
                data += (mu || '') + ',';
                var sigma = $(this).find('span.sigma').html();
                data += sigma || '';
                data += '\n';
            });
    window.location='data:text/csv;charset=utf8,' + encodeURIComponent(data);
}

var updateNumbers = function()
{
    var mu_tot = 0;
    var sigma_tot = 0;
    $('.tasks input[type=number]').each(function(){
        validate($(this));
        });

    $('.tasks ul li.task').each(
           function(){
               var opt = $(this).children('input.optimus').val();
               var norm = $(this).children('input.nominal').val();
               var worst = $(this).children('input.worst').val();
               if(isNumber(opt) && isNumber(norm) && isNumber(worst))
                {
                    var mu = (parseFloat(opt) + 4 * parseFloat(norm) + parseFloat(worst))/6.0;
                    var sigma = (parseFloat(worst) - parseFloat(opt))/6.0;
                    $(this).find('.sum').remove();
                    $('<span class="sum">' + signature + 
                        ' <span class="mu">' + mu.toFixed(2) + 
                        '</span>/<span class="sigma">' + sigma.toFixed(2) + 
                        '</span></span>').insertAfter($(this).find('input.worst'));
                    mu_tot += parseFloat(mu);
                    sigma_tot += sigma * sigma;
					
					
                }
           });
    $('.summary').empty().append($('<span class="totsum">' + 
                totsignature + '<br />' + mu_tot.toFixed(2) + 
                '/' + Math.sqrt(sigma_tot).toFixed(2) + '</span>'));
	$('.percents').empty().
		append($('<span class="25p">' + calcPercentile(25, mu_tot, sigma_tot).toFixed(2) + '</span> ' +
		'<span class="50p">' + calcPercentile(50, mu_tot, sigma_tot).toFixed(2) + '</span> ' +
		'<span class="75p">' + calcPercentile(75, mu_tot, sigma_tot).toFixed(2) + '</span> ' +
		'<span class="95p">' + calcPercentile(95, mu_tot, sigma_tot).toFixed(2) + '</span> ' +
		'<span class="99p">' + calcPercentile(99, mu_tot, sigma_tot).toFixed(2) + '</span>'));
		
}

var validate = function(input)
{
    if(isNumber(input.val()))
    {
        input.removeClass('error');
    }
    else
    {
        input.addClass('error');
    }
}

var init = function()
{
    $('input').live('click',function(){
        $(this).select();
      });

    $('input[type=number]').live('keyup',function(){
        updateNumbers();
      });

    $('img.add').live('click',function(event)
            {
                event.stopImmediatePropagation();
                addTaskTo($(this).closest('li'));
            });
    $('img.delete').live('click',function()
            {
                if($('.tasks ul li.task').size() > 1)
                {
                    $(this).closest('li').remove();
                    updateNumbers();
                }
            });

    $('img.clear').click(
            function(){
                $('.tasks ul li.task').slice(1).remove();
                $('.tasks ul li.task input.optimus').removeClass('error').val('1');
                $('.tasks ul li.task input.nominal').removeClass('error').val('2');
                $('.tasks ul li.task input.worst').removeClass('error').val('3');
                $('.tasks ul li.task input.description').val('description');
                $('input:first').select();
            });

    $('img.download').click(
            function(event)
            {
                popOutCSV();
            });
    $('input').bind('keydown', function(event){
        if(event.keyCode==13 && event.target == this && event.shiftKey)
        {
            var source = $(this).closest('li');
            addTaskTo(source);
            source.find('+ li').find('input:first').select();
        }});

    $('.tasks ul li.task input:first').select();

    $('.catcher').click(
            function()
            {
                $('.explanation').slideToggle('slow');
            });

    $('.tasks ul li.task input:first').select();
}// end init

// statistical functions
var erf = function (x) {
    // By definition erfc(x) = 1 - erf(x), thus it follows that erf(x) = 1 - erfc(x) 
    return (1.0 - erfc(x));
}

var erfc = function (x) {
    // Compute the complementary error function erfc(x).
    // Erfc(x) = (2/sqrt(pi)) Integral(exp(-t^2))dt between x and infinity
    //
    //--- Nve 14-nov-1998 UU-SAP Utrecht

    // The parameters of the Chebyshev fit
    var A1 = -1.26551223;
    var A2 = 1.00002368;
    var A3 = 0.37409196;
    var A4 = 0.09678418;
    var A5 = -0.18628806;
    var A6 = 0.27886807;
    var A7 = -1.13520398;
    var A8 = 1.48851587;
    var A9 = -0.82215223;
    var A10 = 0.17087277;

    var v = 1.0;
    var z = Math.abs(x);

    if (z <= 0) {
        return v; // erfc(0) == 1
    }

    var t = 1.0 / (1.0 + 0.5 * z);
    v = t * Math.exp((-z * z) + A1 + t * (A2 + t * (A3 + t * (A4 + t * (A5 + t * (A6 + t * (A7 + t * (A8 + t * (A9 + t * A10)))))))));

    if (x < 0) {
        v = 2.0 - v; // erfc(-x) == 2 - erfc(x)
    }

    return v;
}

/**
 * The CDFs (cumulative distribution function) of the standard normal distribution.
 *
 * References:
 * http://en.wikipedia.org/wiki/Standard_normal_distribution#Cumulative_distribution_function
 **/
var Phi = function (z) {
    return 0.5 * (1.0 + erf(z / Math.sqrt(2)));
}

var Phi2 = function (z, mu, sigma) {
    return Phi((z - mu) / sigma);
}

/** 
 * The inverse CDF (probit) function of the standard normal distribution.
 * Note that even though it is based on a different approximation algorithm than Phi/erf we achieve a reasonably good
 * fit of up to 9 decimal digits. If you wanted to implement a statistical package in Javascript, you should be cleverer
 * about it and base both erf/erfc and their inverse functions on a common approximation algorithm. For our purpose however,
 * this is more than sufficient.
 *
 * References:
 * http://en.wikipedia.org/wiki/Probit
 * http://home.online.no/~pjacklam/notes/invnorm/
 **/ 
var invPhi = function (p) {
//
  // Lower tail quantile for standard normal distribution function.
  //
  // This function returns an approximation of the inverse cumulative
  // standard normal distribution function.  I.e., given P, it returns
  // an approximation to the X satisfying P = Pr{Z <= X} where Z is a
  // random variable from the standard normal distribution.
  //
  // The algorithm uses a minimax approximation by rational functions
  // and the result has a relative error whose absolute value is less
  // than 1.15e-9.
  //
  // Author:      Peter John Acklam
  // (Javascript version by Alankar Misra @ Digital Sutras (alankar@digitalsutras.com))
  // Time-stamp:  2010-01-21 17:31:15 +01:00
  // E-mail:      pjacklam@online.no
  // WWW URL:     http://home.online.no/~pjacklam

  // An algorithm with a relative error less than 1.15*10-9 in the entire region.

  // Coefficients in rational approximations
  var a = new Array(-3.969683028665376e+01,  2.209460984245205e+02,
                    -2.759285104469687e+02,  1.383577518672690e+02,
                    -3.066479806614716e+01,  2.506628277459239e+00);
  var b = new Array(-5.447609879822406e+01,  1.615858368580409e+02,
                    -1.556989798598866e+02,  6.680131188771972e+01,
                    -1.328068155288572e+01 );
  var c = new Array(-7.784894002430293e-03, -3.223964580411365e-01,
                    -2.400758277161838e+00, -2.549732539343734e+00,
                     4.374664141464968e+00,  2.938163982698783e+00);
  var d = new Array (7.784695709041462e-03,  3.224671290700398e-01,
                     2.445134137142996e+00,  3.754408661907416e+00);

  // Define break-points.
  var plow  = 0.02425;
  var phigh = 1 - plow;

  // Rational approximation for lower region:
  if ( p < plow ) {
           var q  = Math.sqrt(-2*Math.log(p));
           return (((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) /
                                           ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1);
  }

  // Rational approximation for upper region:
  if ( phigh < p ) {
           var q  = Math.sqrt(-2*Math.log(1-p));
           return -(((((c[0]*q+c[1])*q+c[2])*q+c[3])*q+c[4])*q+c[5]) /
                                                  ((((d[0]*q+d[1])*q+d[2])*q+d[3])*q+1);
  }

  // Rational approximation for central region:
  var q = p - 0.5;
  var r = q*q;
  return (((((a[0]*r+a[1])*r+a[2])*r+a[3])*r+a[4])*r+a[5])*q /
                           (((((b[0]*r+b[1])*r+b[2])*r+b[3])*r+b[4])*r+1);
}

var invPhi2 = function (p, mu, sigma) {
	return invPhi(p) * sigma + mu;
}
/* Note: This will only give interpretable results if num tasks > ~25 (20 to 30), 
 * because we assume that the data from all of the tasks combine into a (asymptotically) normal distribution.
 * Even then we stretch the model quite a bit, because it must also hold that the estimates for the individual
 * tasks are unbiased. This is less likely to be the case, if just one person did all the estimates.
 * Yet another reason to employ team-based estimating strategies (such as Planning Poker).
 */
var calcProbability = function (desired, cumAvg, cumStdDev) {
    return 100 * Phi2(desired, cumAvg, cumStdDev);
}

var calcPercentile = function (percent, cumAvg, cumStdDev) {
	var p = percent / 100.0;
	return invPhi2(p, cumAvg, cumStdDev);
}

var round = function (num, dec)
{
  var factor = Math.pow(10,dec);
  return Math.round(num * factor) / factor;
}